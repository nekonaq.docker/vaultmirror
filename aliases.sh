export APP_DIR_VAULTMIRROR="$( /bin/pwd )"

@vaultmirror() {
  ( set -x
    cd "$APP_DIR_VAULTMIRROR"
    docker-compose "$@"
  )
}

@vaultmirror-devel() {
  @vaultmirror -f docker-compose-devel.yml "$@"
}

@vaultmirror.build() {
  @vaultmirror -f docker-compose.yml -f docker-compose-build.yml build "$@"
}

@vaultmirror.push() {
  ( . .env
    set -x
    docker push "$REGISTRY_PREFIX$COMPOSE_PROJECT_NAME:$IMAGE_VERSION"
  )
}

# usage:
#  @vaultmirror up -d
#  @vaultmirror up
#  @vaultmirror run --rm syncget
#  @vaultmirror run --rm syncget centos7
#  @vaultmirror run --rm syncget 7.6.1818/os
#  @vaultmirror run --rm syncget /bin/bash
#
#  @vaultmirror.build
#
#  @vaultmirror-devel up
#  @vaultmirror run --rm syncget
