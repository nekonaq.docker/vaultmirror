FROM buildpack-deps:18.04

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

ENV LANG C.UTF-8

WORKDIR /app

COPY docker-entrypoint /
COPY ./bin "/app/bin"
COPY ./config "/app/config"
COPY requirements.txt "/app"

RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y rsync python3 python3-pip libyaml-dev jq \
&& pip3 install --upgrade pip setuptools \
&& pip3 install -r requirements.txt \
&& mkdir -p /app/data \
&& ( set +x; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ) \
;

ENTRYPOINT ["/docker-entrypoint"]
